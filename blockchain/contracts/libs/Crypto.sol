// SPDX-License-Identifier: MIT
pragma solidity ^0.8.11;

library Crypto {
    struct Sign
    {
        uint8 v;
        bytes32 r;
        bytes32 s;
    }

    function validate(Sign calldata self, bytes32 msgHash) internal pure returns (address) {
        address footprint = ecrecover(msgHash, self.v, self.r, self.s);
        return footprint; // returns signer address in case of successful validation, else returns address(0)
    }
}