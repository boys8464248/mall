// SPDX-License-Identifier: MIT
pragma solidity ^0.8.11;

library Enumerable {
    struct MapAddressToUint {
        mapping(address => uint) map;
        mapping(address => uint) indexes;
        address[] keys;
    }

    function put(MapAddressToUint storage self, address key, uint value) internal {
        if (self.indexes[key] == 0) {
            self.keys.push(key);
            self.indexes[key] = self.keys.length;
        }
        self.map[key] = value;
    }

    function del(MapAddressToUint storage self, address key) internal {
        assert(self.indexes[key] != 0); // cannot delete unexisted key

        // delete from self.keys
        address swapTo = self.keys[self.keys.length - 1];
        uint keyIndex = self.indexes[key] - 1;
        self.keys[keyIndex] = swapTo;
        self.keys.pop();
        self.indexes[swapTo] = keyIndex + 1;

        // delete from other mappings
        delete self.indexes[key];
        delete self.map[key];
    }

    function get(MapAddressToUint storage self, address key) internal view returns (uint) {
        assert(self.indexes[key] != 0); // cannot get unexisted key
        return self.map[key];
    }

    function exists(MapAddressToUint storage self, address key) internal view returns (bool) {
        return self.indexes[key] != 0;
    }

    function listKeys(MapAddressToUint storage self) internal view returns (address[] storage) {
        return self.keys;
    }
}