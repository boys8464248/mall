#!/bin/bash
if [[ "$WAIT_FOR_IT" == "none" ]]; then
    uvicorn final_challenge_backend.app:app --host 0.0.0.0 --port $SOLUTION_BACKEND_PORT
else
    chmod 777 ./wait-for-it.sh
    ./wait-for-it.sh $WAIT_FOR_IT -- uvicorn final_challenge_backend.app:app --host 0.0.0.0 --port $SOLUTION_BACKEND_PORT
fi
