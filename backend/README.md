# final_challenge_backend

![python](https://img.shields.io/badge/python-3.9-blue) [![Code style black](https://img.shields.io/badge/code%20style-black-black)](https://github.com/psf/black)

## Setup

Clone repo.

Install dependencies:
```sh
poetry env use 3.9
poetry install --no-root
```

Activate venv:
```sh
poetry shell
```

Install pre-commit hooks (optional):
```sh
pre-commit install
```

Start dev server:
```sh
invoke dev
```

See urls for references and docs:
- http://localhost:8000/docs
- http://localhost:8000/graphql (you need auth in browser. Recommend [Requestly](https://chrome.google.com/webstore/detail/requestly-redirect-url-mo/mdnleldcmiljblolnjhpnblkcekpdkpa) extension for Chrome)
