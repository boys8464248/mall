__version__ = "0.1.0"

import os

LANDLORD = os.getenv("LANDLORD_ADDRESS")
RPC_ENDPOINT = os.getenv("RPC_URL")
