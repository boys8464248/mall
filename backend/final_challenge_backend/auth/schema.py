from typing import Optional

import graphene
from eth_utils import is_checksum_address
from graphql import ResolveInfo
from sqlalchemy.ext.asyncio import AsyncSession

from final_challenge_backend.auth.utils import generate_string, validate_message, check_token, current_user_is_landlord
from final_challenge_backend.db.models import User, get, get_or_create, Role, get_user_role
from final_challenge_backend.tickets.schema import InputSignature
from final_challenge_backend.utils import _get_engine


# type Authentication {
#     address: String!
#     isLandlord: Boolean!
# }
class Authentication(graphene.ObjectType):
    address = graphene.String(required=True)
    is_landlord = graphene.Boolean(required=True)


# authentication: Authentication
class AuthsQuery(graphene.ObjectType):
    authentication = graphene.Field(Authentication)

    async def resolve_authentication(self, info: ResolveInfo) -> Optional[Authentication]:
        engine = await _get_engine(info)
        request = info.context["request"]

        if not check_token(request.authorize):
            return
        token = str(request.cookies["access_token_cookie"])

        async with AsyncSession(engine) as session:
            user = await get(session, User, access_token=token)
            is_landlord = await current_user_is_landlord(session, info)

        return Authentication(address=user.address,
                              is_landlord=is_landlord)


class RequestAuth(graphene.Mutation):
    class Arguments:
        address = graphene.String(required=True)

    Output = graphene.String

    async def mutate(root, info, address):  # type: ignore
        engine = await _get_engine(info)

        if not is_checksum_address(address):
            raise Exception("Invalid address")

        async with AsyncSession(engine, expire_on_commit=False) as session:
            user = await get_or_create(session, User, address=address)
            message = generate_string()
            user.message = message
            await session.commit()
        return message


class Auth(graphene.Mutation):
    class Arguments:
        address = graphene.String(required=True)
        signed_message = InputSignature(required=True)

    Output = Authentication

    async def mutate(root, info, address, signed_message):  # type: ignore
        engine = await _get_engine(info)

        if not is_checksum_address(address):
            raise Exception("Invalid address")

        async with AsyncSession(engine, expire_on_commit=False) as session:
            user = await get(session, User, address=address)
            auth_exception = Exception("Authentication failed")
            if not user:
                raise auth_exception
            if not validate_message(user.message, signed_message, address):
                raise auth_exception
            Authorize = info.context["request"].authorize
            access_token = Authorize.create_access_token(subject=address)
            info.context["request"].set_token_cookie = access_token
            user.access_token = access_token

            user.message = generate_string()
            session.add(user)
            await session.commit()

            role = await get_user_role(session, user)
        return Authentication(address=address,
                              is_landlord=role == Role.LANDLORD)


class AuthMutations(graphene.ObjectType):
    request_authentication = RequestAuth.Field()
    authenticate = Auth.Field()
