import random
import string
from typing import Optional

from eth_account import Account
from eth_account.messages import encode_defunct
from fastapi_jwt_auth import AuthJWT
from fastapi_jwt_auth.exceptions import AuthJWTException
from graphql import ResolveInfo
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from final_challenge_backend.db.models import Role, get_user_role, User, Room, get
from final_challenge_backend.w3.tools import get_contract


def validate_message(message: str, signed_message, address: str) -> bool:
    msg_hash = encode_defunct(text=message)
    sign = signed_message
    try:
        vrs = (int(sign.v, base=16), int(sign.r, base=16), int(sign.s, base=16))
    except ValueError:
        return False
    try:
        recovered_address: str = Account.recover_message(
            msg_hash, vrs=vrs
        )
    except Exception:
        return False
    return address == recovered_address


def check_token(Authorize: AuthJWT) -> bool:
    try:
        Authorize.jwt_required()
    except AuthJWTException:
        return False
    return True


def auth_required(info: ResolveInfo):
    request = info.context["request"]
    if request.app.state.config.debug:
        return

    if not check_token(request.authorize):
        raise Exception("Authentication required")


def generate_string(message_len: int = 20) -> str:
    return "".join(random.choices(string.ascii_letters + string.digits, k=message_len))


def current_user_token(info: ResolveInfo):
    request = info.context["request"]
    if not check_token(request.authorize):
        return None
    return str(request.cookies["access_token_cookie"])


async def current_user(session: AsyncSession, info: ResolveInfo) -> Optional[User]:
    token = current_user_token(info)
    if token is None:
        return None
    return await get(session, User, access_token=token)


async def current_user_is_landlord(session: AsyncSession, info: ResolveInfo) -> bool:
    if info.context["request"].app.state.config.debug:
        return True

    user = await current_user(session, info)
    role = await get_user_role(session, user)

    return user is not None and role == Role.LANDLORD


async def current_user_is_tenant(session: AsyncSession, info: ResolveInfo, room: Optional[Room] = None) -> bool:
    request = info.context["request"]
    if request.app.state.config.debug:
        return True
    if room is not None and not room.contract_address:
        return False

    user = await current_user(session, info)
    if user is None:
        return False

    if room is None:
        statement = select(Room).where(
            Room.contract_address is not None and
            Room.contract_address != "")
        query_res = await session.execute(statement)
        rooms = query_res.scalars().all()
    else:
        rooms = [room]

    for room in rooms:
        contract = get_contract(request.app.web3, room.contract_address)
        tenant_address = contract.functions.getTenant().call()
        if tenant_address == user.address:
            return True

    return False
