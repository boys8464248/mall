from web3.exceptions import ContractLogicError


def is_revert_error(msg: str, exc: ContractLogicError):
    exc = str(exc)
    msg = "revert " + msg
    return msg == exc[-len(msg):]


def is_integer(s: str):
    if s[0] in ('-', '+'):
        s = s[1:]
    if s.isdecimal():
        try:
            s = int(s)
            return True
        except Exception:
            return False
    return False
