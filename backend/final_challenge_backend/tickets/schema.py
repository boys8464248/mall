from typing import Union

import graphene
from dateutil import parser
from graphql import ResolveInfo
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload
from web3 import Web3
from web3.exceptions import ContractLogicError

from final_challenge_backend.auth.utils import auth_required, current_user
from final_challenge_backend.db.models import Ticket as TicketModel, get, Room
from final_challenge_backend.rooms.schema import RoomSchema
from final_challenge_backend.tickets.utils import is_revert_error, is_integer
from final_challenge_backend.utils import _get_engine
from final_challenge_backend.w3.tools import get_contract


# input InputWei {
#     wei: String!
# }
class InputWei(graphene.InputObjectType):
    wei = graphene.String(required=True)


# type Wei {
#     wei: String!
# }
class Wei(graphene.ObjectType):
    wei = graphene.String(required=True)


# input InputSignature {
#     v: Int!
#     r: Int!
#     s: Int!
# }
class InputSignature(graphene.InputObjectType):
    v = graphene.String(required=True)
    r = graphene.String(required=True)
    s = graphene.String(required=True)


# type Signature {
#     v: String!
#     r: String!
#     s: String!
# }
class Signature(graphene.ObjectType):
    v = graphene.String(required=True)
    r = graphene.String(required=True)
    s = graphene.String(required=True)


# input InputNonce {
#     value: String!
# }
class InputNonce(graphene.InputObjectType):
    value = graphene.String(required=True)


# type Nonce {
#     value: String!
# }
class Nonce(graphene.ObjectType):
    value = graphene.String(required=True)


# input InputDatetime {
#     datetime: String!
# }
class InputDatetime(graphene.InputObjectType):
    datetime = graphene.String(required=True)


# type Datetime {
#     datetime: String!
# }
class Datetime(graphene.ObjectType):
    datetime = graphene.String(required=True)


# input InputTicket {
#     room: ID!
#     nonce: InputNonce!
#     value: InputWei!
#     deadline: InputDatetime!
#     cashierSignature: InputSignature!
# }
class InputTicket(graphene.InputObjectType):
    room = graphene.ID(required=True)
    nonce = graphene.Field(InputNonce, required=True)
    value = graphene.Field(InputWei, required=True)
    deadline = graphene.Field(InputDatetime, required=True)
    cashier_signature = graphene.Field(InputSignature, required=True)


def input_ticket_hash(input_ticket):
    sign = input_ticket.cashier_signature
    sign_str = sign.v + sign.s + sign.r
    d = {
        "room": input_ticket.room,
        "nonce": input_ticket.nonce.value,
        "value": input_ticket.value.wei,
        "deadline": parser.isoparse(input_ticket.deadline.datetime).timestamp(),
        "cashierSignature": sign_str
    }
    return abs(hash(frozenset(d.items())))


# type Ticket {
#     id: ID!
#     room: Room!
#     value: Wei!
#     deadline: Datetime!
#     nonce: Nonce!
#     cashierSignature: Signature!
# }
class Ticket(graphene.ObjectType):
    id = graphene.ID(required=True)
    room = graphene.Field(RoomSchema, required=True)
    value = graphene.Field(Wei, required=True)
    deadline = graphene.Field(Datetime, required=True)
    nonce = graphene.Field(Nonce, required=True)
    cashier_signature = graphene.Field(Signature, required=True)


def model_to_ticket(ticket: TicketModel) -> Ticket:
    return Ticket(
        id=ticket.id,
        room=ticket.room,
        value=Wei(wei=ticket.value),
        deadline=Datetime(datetime=ticket.deadline),
        nonce=Nonce(value=ticket.nonce),
        cashier_signature=Signature(
            s=ticket.s,
            v=ticket.v,
            r=ticket.r,
        )
    )


# ticket(id: ID!): Ticket!
class TicketsQuery(graphene.ObjectType):
    ticket = graphene.Field(Ticket, required=True, id=graphene.NonNull(graphene.ID))

    async def resolve_ticket(self, info: ResolveInfo, id: Union[int, str]) -> Ticket:
        # auth_required(info)
        engine = await _get_engine(info)
        async with AsyncSession(engine) as session:
            statement = select(TicketModel) \
                .options(selectinload(TicketModel.room)) \
                .filter_by(id=id)
            query_res = await session.execute(statement)
            ticket: TicketModel = query_res.scalars().one_or_none()
            if ticket is None:
                raise Exception(f"Ticket with such ID not found")
        return model_to_ticket(ticket)


# createTicket(ticket: InputTicket!): Ticket!
class CreateTicket(graphene.Mutation):
    class Arguments:
        ticket = graphene.NonNull(InputTicket)

    Output = graphene.NonNull(Ticket)

    async def mutate(root, info: ResolveInfo, ticket):
        auth_required(info)
        engine = await _get_engine(info)
        input_ticket = ticket

        async with AsyncSession(engine) as session:
            # existing_ticket = await get(session, TicketModel, id=input_ticket.id)
            # if existing_ticket is not None:
            #     raise Exception("Ticket already exists")
            w3: Web3 = info.context["request"].app.web3

            room: Room = await get(session, Room, id=input_ticket.room)
            if room is None:
                raise Exception("Room with such ID not found")
            if not is_integer(input_ticket.value.wei):
                raise Exception("Value must be an integer")
            if int(input_ticket.value.wei) < 0:
                raise Exception("Value must be greater than zero")
            contract_address = room.contract_address
            if contract_address in [None, "", "0x"]:
                raise Exception("Room does not have a contract")
            contract = get_contract(w3, contract_address)
            if contract.address in [None, "", "0x"]:
                raise Exception("Room does not have a contract")

            user = await current_user(session, info)
            cashiers = contract.functions.getCashiersList().call()
            if user.address not in cashiers:
                raise Exception("This method is available only for the cashiers")

            sign = input_ticket.cashier_signature
            print(sign)
            try:
                sign_ints = (int(sign.v, base=16), int(sign.r, base=16), int(sign.s, base=16))
                sign_parsed = (sign_ints[0], w3.toBytes(sign_ints[1]), w3.toBytes(sign_ints[2]))
                assert 0 <= sign_ints[0] < 2**8
            except (ValueError, AssertionError):
                raise Exception("Invalid cashier signature")
            try:
                deadline = parser.isoparse(input_ticket.deadline.datetime).timestamp()
            except ValueError:
                raise Exception("Invalid deadline date format")

            try:
                print(contract.address == contract_address, contract_address, contract.address)
                res = contract.functions.pay(
                    int(deadline),
                    int(input_ticket.nonce.value),
                    int(input_ticket.value.wei),
                    sign_parsed
                ).call({
                    "value": int(input_ticket.value.wei)
                })
                print("OK: ", res)
            except ContractLogicError as e:
                print(e)
                print(e.args)
                print(type(e))
                print(dir(e))
                if is_revert_error("Invalid nonce", e):
                    raise Exception("Invalid nonce")
                if is_revert_error("Unknown cashier", e):
                    raise Exception("Unknown cashier")
                if is_revert_error("The operation is outdated", e):
                    raise Exception("The operation is outdated")
                raise e

            _id = input_ticket_hash(input_ticket)

            ticket_model = TicketModel(
                id=_id,
                room_id=input_ticket.room,
                value=input_ticket.value.wei,
                deadline=input_ticket.deadline.datetime,
                nonce=input_ticket.nonce.value,
                r=sign.r,
                v=sign.v,
                s=sign.s
            )

            session.add(ticket_model)
            await session.commit()

        async with AsyncSession(engine, expire_on_commit=False) as session:
            statement = select(TicketModel) \
                .options(selectinload(TicketModel.room)) \
                .filter_by(id=_id)
            query_res = await session.execute(statement)
            ticket_model = query_res.scalars().one_or_none()
        return model_to_ticket(ticket_model)


class TicketsMutation(graphene.ObjectType):
    create_ticket = CreateTicket.Field()
