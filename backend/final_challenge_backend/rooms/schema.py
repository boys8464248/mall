from typing import List, Union

import graphene
from eth_utils import is_checksum_address
from graphene_sqlalchemy import SQLAlchemyObjectType
from graphql.execution.base import ResolveInfo
from hexbytes import HexBytes
from sqlalchemy import select, delete
from sqlalchemy.ext.asyncio.session import AsyncSession

from final_challenge_backend.auth.utils import current_user_is_landlord, auth_required, current_user_is_tenant, \
    current_user
from final_challenge_backend.db.models import Room, get
from final_challenge_backend.utils import _get_engine
from final_challenge_backend.w3.tools import get_contract, contract_is_rented, contract_is_ended


class RoomSchema(SQLAlchemyObjectType):
    class Meta:
        model = Room
        exclude_fields = ["location_id"]


# rooms: [Room!]!
# room(id: ID!): Room!
class RoomsQuery(graphene.ObjectType):
    rooms = graphene.NonNull(graphene.List(graphene.NonNull(RoomSchema)))
    room = graphene.Field(RoomSchema, required=True, id=graphene.NonNull(graphene.ID))

    async def resolve_room(self, info: ResolveInfo, id: Union[int, str]) -> Room:
        auth_required(info)
        engine = await _get_engine(info)
        async with AsyncSession(engine) as session:
            room = await get(session, Room, id=id)
            if room is None:
                raise Exception(f"Room with such ID not found")
        return room

    async def resolve_rooms(self, info: ResolveInfo) -> List[Room]:
        engine = await _get_engine(info)

        async with AsyncSession(engine) as session:
            is_landlord = await current_user_is_landlord(session, info)
            user = await current_user(session, info)
            # if not is_landlord and not await current_user_is_tenant(session, info):
            #     print("Not landlord neither tenant: ", user.address)
            #     statement = select(Room).where(
            #         Room.contract_address is None or
            #         Room.contract_address == "")
            #     return (await session.execute(statement)).scalars().all()
            query_result = await session.execute(select(Room))
            rooms: List[Room] = query_result.scalars().all()

        if is_landlord:
            return rooms

        # filter out rooms of other tenant
        w3 = info.context["request"].app.web3
        new_rooms = []
        for room in rooms:
            print(room, room.contract_address)
            if room.contract_address in [None, "", "0x"]:
                continue
            contract = get_contract(w3, room.contract_address)
            tenant_address = 'INVALID_ADDRESS'
            try:
                tenant_address = contract.functions.getTenant().call()
                print(f"ADDRESS OF TENANT", tenant_address, dir(tenant_address))
            except Exception as e:
                print(e)
            if tenant_address == user.address or \
                    (not contract_is_rented(w3, room.contract_address)
                     and not contract_is_ended(w3, room.contract_address)):
                new_rooms.append(room)
        return new_rooms


# Good idea, but cannot control exception message
# class PositiveFloat(graphene.Scalar):
#     @staticmethod
#     def coerce_float(value):
#         res = graphene.Float.coerce_float(value)
#         if res is not None and res > 0:
#             return res
#
#     serialize = coerce_float
#     parse_value = coerce_float
#
#     @staticmethod
#     def parse_literal(ast):
#         value = graphene.Float.parse_literal(ast)
#         if value is not None and value > 0:
#             return value


# input InputRoom {
#     internalName: String!
#     area: Float!
#     location: String!
# }
class InputRoom(graphene.InputObjectType):
    internal_name = graphene.String(required=True)
    area = graphene.Float(required=True)
    location = graphene.String(required=True)


async def landlord_required(session, info):
    if not await current_user_is_landlord(session, info):
        raise Exception("This method is available only for the landlord")


class CreateRoom(graphene.Mutation):
    class Arguments:
        room = InputRoom(required=True)

    Output = graphene.NonNull(RoomSchema)

    async def mutate(root, info: ResolveInfo, room):
        auth_required(info)
        engine = await _get_engine(info)
        input_room = room

        async with AsyncSession(engine, expire_on_commit=False) as session:
            await landlord_required(session, info)

            room = await get(session, Room, internal_name=input_room.internal_name)
            if room is not None:
                raise Exception("Room already exists")

            if input_room.area <= 0:
                raise Exception("The room area must be greater than zero")

            _id = str(abs(hash(input_room.internal_name)))
            room = Room(
                id=_id,
                **input_room)
            session.add(room)
            await session.commit()
        return room


class SetRoomContractAddress(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)
        contract_address = graphene.String(default_value=None)

    Output = graphene.NonNull(RoomSchema)

    async def mutate(root, info: ResolveInfo, id, **kwargs):
        auth_required(info)
        engine = await _get_engine(info)

        contract_address = kwargs.get('contract_address', None)
        w3 = info.context["request"].app.web3

        if contract_address is not None:
            if not is_checksum_address(contract_address):
                raise Exception("Contract with such address not found")

            bytecode: HexBytes = w3.eth.get_code(contract_address)

            # print(bytecode.hex())
            # print(get_bytecode())
            # Can be tested like this. However, solution must know exact bytecode of the contract
            # if bytecode.hex() != get_bytecode():
            if bytecode.hex() in ('', '0x'):
                raise Exception("Contract with such address not found")

        async with AsyncSession(engine, expire_on_commit=False) as session:
            await landlord_required(session, info)

            room = await get(session, Room, id=id)
            if room is None:
                raise Exception(f"Room with such ID not found")

            if contract_is_rented(w3, room.contract_address):
                raise Exception("Room has rented contract in progress")

            room.contract_address = contract_address

            session.add(room)
            await session.commit()

        return room


class SetRoomPublicName(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)
        public_name = graphene.String(default_value=None)

    Output = graphene.NonNull(RoomSchema)

    async def mutate(root, info: ResolveInfo, id, **kwargs):
        auth_required(info)
        engine = await _get_engine(info)

        public_name = kwargs.get('public_name', None)

        async with AsyncSession(engine, expire_on_commit=False) as session:
            room = await get(session, Room, id=id)
            if room is None:
                raise Exception(f"Room with such ID not found")

            if not await current_user_is_landlord(session, info) \
                    and not await current_user_is_tenant(session, info, room):
                raise Exception("This room is not rented by you")

            room.public_name = public_name

            session.add(room)
            await session.commit()
        return room


class EditRoom(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)
        room = InputRoom(required=True)

    Output = graphene.NonNull(RoomSchema)

    async def mutate(root, info: ResolveInfo, id, room):
        auth_required(info)
        engine = await _get_engine(info)
        input_room = room

        async with AsyncSession(engine, expire_on_commit=False) as session:
            await landlord_required(session, info)

            room = await get(session, Room, id=id)
            if room is None:
                raise Exception(f"Room with such ID not found")

            if input_room.area <= 0:
                raise Exception("The room area must be greater than zero")

            for k, v in input_room.items():
                setattr(room, k, v)

            session.add(room)
            await session.commit()
        return room


class RemoveRoom(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)

    Output = graphene.NonNull(RoomSchema)

    async def mutate(root, info: ResolveInfo, id):
        auth_required(info)
        engine = await _get_engine(info)
        w3 = info.context["request"].app.web3

        async with AsyncSession(engine, expire_on_commit=False) as session:
            await landlord_required(session, info)

            room = await get(session, Room, id=id)
            if room is None:
                raise Exception(f"Room with such ID not found")

            if contract_is_rented(w3, room.contract_address):
                raise Exception("Room has rented contract in progress")

            statement = delete(Room).where(Room.id == id)
            await session.execute(statement)

            await session.commit()
        return room


class RoomsMutation(graphene.ObjectType):
    create_room = CreateRoom.Field()
    set_room_contract_address = SetRoomContractAddress.Field()
    set_room_public_name = SetRoomPublicName.Field()
    edit_room = EditRoom.Field()
    remove_room = RemoveRoom.Field()
