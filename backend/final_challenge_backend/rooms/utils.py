from typing import List

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from final_challenge_backend.db.models import Room
from web3 import Web3
from web3.contract import Contract

from final_challenge_backend.w3.tools import get_contract


async def get_contracts(session: AsyncSession, w3: Web3) -> List[Contract]:
    statement = select(Room).where(Room.contract_address != "" and
                                   Room.contract_address is not None)
    rooms_with_contracts: List[Room] = (await session.execute(statement)).scalars().all()
    return [get_contract(w3, r.contract_address) for r in rooms_with_contracts]


