from typing import List, Optional

import graphene
from graphene_sqlalchemy import SQLAlchemyObjectType
from graphql.execution.base import ResolveInfo
from sqlalchemy import select
from sqlalchemy.ext.asyncio.session import AsyncSession

from final_challenge_backend.auth.schema import AuthMutations, AuthsQuery
from final_challenge_backend.auth.utils import auth_required
from final_challenge_backend.db.models import User
from final_challenge_backend.rooms.schema import RoomsQuery, RoomsMutation
from final_challenge_backend.tickets.schema import TicketsMutation, TicketsQuery
from final_challenge_backend.utils import _get_engine


class UserSchema(SQLAlchemyObjectType):
    class Meta:
        model = User
        exclude_fields = ["password"]


class UsersQuery(graphene.ObjectType):
    users = graphene.List(UserSchema, ids=graphene.List(graphene.Int))

    async def resolve_users(
            self, info: ResolveInfo, ids: Optional[List[int]] = None
    ) -> List[User]:
        auth_required(info)
        engine = await _get_engine(info)
        statement = select(User)
        if ids:
            statement = statement.filter(User.id.in_(ids))
        async with AsyncSession(engine) as session:
            query_result = await session.execute(statement)
        users: List[User] = query_result.scalars().fetchall()
        return users


class RootQuery(UsersQuery, RoomsQuery, AuthsQuery, TicketsQuery, graphene.ObjectType):
    pass


class RootMutation(RoomsMutation, AuthMutations, TicketsMutation, graphene.ObjectType):
    pass


schema = graphene.Schema(query=RootQuery, mutation=RootMutation)
