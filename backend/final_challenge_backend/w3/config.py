from web3 import Web3


def create_web3(config) -> Web3:
    w3 = Web3(Web3.HTTPProvider(config.rpc_endpoint))
    return w3



