import json
from enum import Enum

from web3 import Web3
from web3.contract import Contract


class ContractState(Enum):
    READY_FOR_RENT = 0
    RENTED = 1
    ENDED = 2


def contract_is_rented(w3: Web3, address: str) -> bool:
    if address not in [None, "", "0x"]:
        contract = get_contract(w3, address)
        state = ContractState(contract.functions.getState().call())
        return state == ContractState.RENTED
    return False


def contract_is_ended(w3: Web3, address: str) -> bool:
    if address not in [None, "", "0x"]:
        contract = get_contract(w3, address)
        state = ContractState(contract.functions.getState().call())
        return state == ContractState.ENDED
    return False


def get_contract(w3: Web3, address: str) -> Contract:
    with open("final_challenge_backend/w3/abi.json", "r") as f:
        abi = json.load(f)
    contract = w3.eth.contract(address=address, abi=abi)
    return contract


def get_bytecode() -> str:
    with open("final_challenge_backend/w3/bytecode.json", "r") as f:
        bytecode = json.load(f)
    return "0x" + bytecode["object"]
