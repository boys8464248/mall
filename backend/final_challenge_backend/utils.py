import asyncio
from typing import Any, Coroutine, List

from graphql import ResolveInfo
from sqlalchemy.ext.asyncio import AsyncEngine
from starlette.requests import Request
from starlette.responses import JSONResponse

from final_challenge_backend.types import BaseError


async def base_error_handler(request: Request, exception: BaseError) -> JSONResponse:
    status_code = getattr(exception, "status_code", 500)

    return JSONResponse(
        {"code": exception.code, "message": exception.message}, status_code=status_code
    )


async def gather_with_concurrency(
    count: int, tasks: List[Coroutine[Any, Any, Any]]
) -> List[Any]:
    semaphore = asyncio.Semaphore(count)

    async def semaphore_task(task: Coroutine[Any, Any, Any]) -> Any:
        async with semaphore:
            return await task

    result: List[Any] = await asyncio.gather(
        *(semaphore_task(task) for task in tasks), return_exceptions=True
    )

    return result


async def _get_engine(info: ResolveInfo) -> AsyncEngine:
    request: Request = info.context["request"]  # type: ignore
    engine = request.app.state.engine
    return engine
