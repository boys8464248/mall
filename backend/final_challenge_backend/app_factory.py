from typing import Callable, Coroutine, cast

from fastapi import FastAPI
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware
from uvicorn.middleware.proxy_headers import ProxyHeadersMiddleware

from final_challenge_backend.config import Configuration, setup_sentry
from final_challenge_backend.db.utils import get_engine, init_database
from final_challenge_backend.logging import setup_logging
from final_challenge_backend.routes import api_router
from final_challenge_backend.types import AppState, BaseError
from final_challenge_backend.utils import base_error_handler
from final_challenge_backend.w3.config import create_web3


class BackendApp(FastAPI):
    state: AppState  # type: ignore


def create_startup_hook(app: BackendApp) -> Callable[[], Coroutine[None, None, None]]:
    async def startup_hook() -> None:
        app.state.engine = get_engine(app.state.config.database)
        if app.state.config.database.create_on_run:
            await init_database(app.state.engine)

    return startup_hook


def create_shutdown_hook(app: BackendApp) -> Callable[[], Coroutine[None, None, None]]:
    async def shutdown_hook() -> None:
        if app.state.engine is not None:
            await app.state.engine.dispose()

    return shutdown_hook


exception_handlers = {BaseError: base_error_handler}


def create_app(config: Configuration) -> BackendApp:
    setup_logging(config)
    setup_sentry(config)

    app = cast(
        BackendApp,
        FastAPI(
            title="GraphQLBackend",
            description="GraphQL App backend",
            debug=config.debug,
            exception_handlers=exception_handlers,  # type: ignore
        ),
    )
    app.state.config = config
    app.web3 = create_web3(config)

    app.include_router(api_router)

    app.router.add_event_handler("startup", create_startup_hook(app))
    app.router.add_event_handler("shutdown", create_shutdown_hook(app))

    app.add_middleware(ProxyHeadersMiddleware)
    app.add_middleware(SentryAsgiMiddleware)

    return app
