from enum import Enum
from typing import Any, Optional

from sqlalchemy import Boolean, Column, Float, Integer, String, select, ForeignKey
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import declarative_base, relationship

from final_challenge_backend import LANDLORD as LANDLORD_ADDRESS

Base = declarative_base()


class Role(Enum):
    NO_ROLE = 0
    LANDLORD = 1


class User(Base):  # type: ignore
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    address = Column(String, unique=True, nullable=False)
    message = Column(String)
    is_active = Column(Boolean, default=True, nullable=False)
    access_token = Column(String, nullable=True)


# type Room {
#     id: ID!
#     internalName: String!
#     area: Float!
#     location: String!
#     publicName: String
#
#     contractAddress: String
# }
class Room(Base):  # type: ignore
    __tablename__ = "rooms"

    id = Column(String, primary_key=True)
    internal_name = Column(String, unique=True, nullable=False)
    area = Column(Float, nullable=False)
    location = Column(String, nullable=False)
    public_name = Column(String)

    contract_address = Column(String)


# type Ticket {
#     id: ID!
#     room: Room!
#     value: Wei!
#     deadline: Datetime!
#     nonce: Nonce!
#     cashierSignature: Signature!
# }
class Ticket(Base):
    __tablename__ = "tickets"

    id = Column(String, primary_key=True)

    room_id = Column(Integer, ForeignKey('rooms.id'))
    room = relationship("Room")

    value = Column(String, nullable=False)
    deadline = Column(String, nullable=False)
    nonce = Column(String, nullable=False)

    v = Column(String, nullable=False)
    r = Column(String, nullable=False)
    s = Column(String, nullable=False)


async def get_or_create(session: AsyncSession, model: Any, **kwargs) -> Any:
    instance = await get(session, model, **kwargs)

    if instance:
        return instance
    else:
        instance = model(**kwargs)
        session.add(instance)
        return instance


async def get(session: AsyncSession, model: Any, **kwargs) -> Optional[Any]:
    statement = select(model).filter_by(**kwargs)
    query_res = await session.execute(statement)
    instance = query_res.scalars().one_or_none()
    return instance


async def get_user_role(session: AsyncSession, user: Optional[User]) -> Role:
    if user is None:
        return Role.NO_ROLE

    if user.address == LANDLORD_ADDRESS:
        return Role.LANDLORD

    return Role.NO_ROLE
