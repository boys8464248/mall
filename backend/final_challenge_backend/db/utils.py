from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.ext.asyncio.engine import AsyncEngine

from final_challenge_backend.config import DatabaseConfiguration, load_configuration
from final_challenge_backend.db.models import Base


def get_engine(config: DatabaseConfiguration) -> AsyncEngine:
    return create_async_engine(config.dsn)


async def init_database(engine: AsyncEngine) -> None:
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)
