from fastapi import APIRouter, Depends, Request, Response
from fastapi_jwt_auth import AuthJWT
from graphql.execution.executors.asyncio import AsyncioExecutor
from pydantic import BaseModel
from starlette.graphql import GraphQLApp
from starlette.responses import PlainTextResponse

from final_challenge_backend.config import load_configuration
from final_challenge_backend.schema import schema


class Token(BaseModel):
    access_token: str
    token_type: str


api_router = APIRouter()
graphql_app = GraphQLApp(schema=schema, executor_class=AsyncioExecutor)


class Settings(BaseModel):
    authjwt_secret_key: str = load_configuration().secret_key
    authjwt_token_location: set = {"cookies"}
    authjwt_cookie_csrf_protect: bool = False


# callback to get your configuration
@AuthJWT.load_config
def get_config() -> Settings:
    return Settings()


@api_router.api_route("/graphql", methods=["GET"], response_class=PlainTextResponse)
async def ping() -> str:
    return "OK"


@api_router.api_route("/graphql", methods=["POST"])
async def graphql_endpoint(
    request: Request, Authorize: AuthJWT = Depends()
) -> Response:
    setattr(request, "authorize", Authorize)
    resp = await graphql_app.handle_graphql(request)
    if token := getattr(request, "set_token_cookie", None):
        Authorize.set_access_cookies(token, response=resp)
    return resp
