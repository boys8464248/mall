FROM python:3.9-slim as python-base

ENV ENV_FOR_DYNACONF=docker \
    PYTHONUNBUFFERED=1 \
    POETRY_HOME="/opt/poetry" \
    POETRY_NO_INTERACTION=1 \
    PYSETUP_PATH="/opt/pysetup" \
    VENV_PATH="/opt/pysetup/.venv"

ENV PATH="$POETRY_HOME/bin:$VENV_PATH/bin:$PATH"


FROM python-base as builder-base

RUN apt-get update \
    && apt-get install --no-install-recommends -y \
        curl \
        build-essential

ENV POETRY_VERSION=1.1.13
RUN curl -sSL https://install.python-poetry.org | python

ENV POETRY_VIRTUALENVS_IN_PROJECT=true
WORKDIR $PYSETUP_PATH
COPY ./poetry.lock ./pyproject.toml ./
RUN poetry install --no-interaction --no-ansi --no-dev


FROM python-base as production

RUN pip install python-dateutil
COPY --from=builder-base $VENV_PATH $VENV_PATH

COPY ./docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh

WORKDIR /code
COPY ./config /code/config
COPY ./final_challenge_backend /code/final_challenge_backend
COPY ./start-in-docker.sh /code/start-in-docker.sh
COPY ./wait-for-it.sh /code/wait-for-it.sh

ENV WAIT_FOR_IT=none

ENTRYPOINT /docker-entrypoint.sh $0 $@
CMD ["./start-in-docker.sh"]
