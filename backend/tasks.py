# type: ignore
from invoke import task


@task
def dev(c):
    """
    Run development server
    """
    c.run("python -m final_challenge_backend.app", pty=True)
