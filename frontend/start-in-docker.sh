#!/bin/bash
if [[ "$WAIT_FOR_IT" == "none" ]]; then
    nginx -g "daemon off;"
else
    chmod 777 ./wait-for-it.sh
    ./wait-for-it.sh $WAIT_FOR_IT -- nginx -g "daemon off;"
fi
