npx solcjs --bin --abi --output-dir ./src/contract/tmp --base-path src/contract src/contract/RentalAgreement.sol

cd src/contract
mv tmp/RentalAgreement_sol_RentalAgreement.abi RentalAgreement.json
mv tmp/RentalAgreement_sol_RentalAgreement.bin RentalAgreement.bin
rm -r tmp

