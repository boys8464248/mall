import { providers } from "ethers";
import { EventEmitter } from "stream";

declare global {
  interface Window {
    ethereum: providers.ExternalProvider & EventEmitter;
  }
}
