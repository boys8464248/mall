import {
  BigNumber,
  Contract,
  ContractFactory,
  providers,
  Signer,
} from "ethers";

import abi from "./RentalAgreement.json";
import bytecodeUrl from "./RentalAgreement.bin";
import { PayParameters, RentParameters, RoomStatus } from "../types";

let bytecode: string;
export const fetchBytecode = async () => {
  if (!bytecode) {
    const response = await fetch(bytecodeUrl);
    bytecode = await response.text();
  }
  return bytecode;
};

type SignerOrProvider =
  | { signer: Signer }
  | { provider: providers.Web3Provider };
type RentalAgreementProps = SignerOrProvider & { address: string };

export class RentalAgreement {
  inner: Contract;
  constructor(props: RentalAgreementProps) {
    this.inner = new Contract(
      props.address,
      abi,
      `signer` in props ? props.signer : props.provider
    );
  }

  async getState(): Promise<Exclude<RoomStatus, `rentUnavailable`>> {
    const raw: number = await this.inner.getState();
    switch (raw) {
      case 0:
        return `rentAvailable`;
      case 1:
        return `rented`;
      case 2:
        return `rentEnded`;
      default:
        throw new Error(`Unknown value for State`);
    }
  }

  async getTenant(): Promise<string> {
    const tenant = await this.inner.getTenant();
    return tenant;
  }

  async getRentStartTime(): Promise<Date> {
    const timestamp: BigNumber = await this.inner.getRentStartTime();
    return new Date(timestamp.toNumber() * 1000);
  }

  async getRentEndTime(): Promise<Date> {
    const timestamp: BigNumber = await this.inner.getRentEndTime();
    return new Date(timestamp.toNumber() * 1000);
  }

  async getBillingPeriodDuration(): Promise<number> {
    const period: BigNumber = await this.inner.getBillingPeriodDuration();
    return period.toNumber();
  }

  async getRentalRate(): Promise<BigNumber> {
    const rate = await this.inner.getRentalRate();
    return rate;
  }

  async rent({
    deadline,
    tenant,
    rentalRate,
    billingPeriodDuration,
    billingsCount,
    sign,
  }: RentParameters): Promise<void> {
    const transaction = await this.inner.rent(
      deadline,
      tenant,
      rentalRate,
      billingPeriodDuration,
      billingsCount,
      sign,
      { value: rentalRate }
    );
    await transaction.wait();
  }

  async getCashiersList(): Promise<Set<string>> {
    const cashiers: string[] = await this.inner.getCashiersList();
    return new Set(cashiers);
  }

  async addCashier(cashier: string): Promise<void> {
    const transaction = await this.inner.addCashier(cashier);
    await transaction.wait();
  }

  async removeCashier(cashier: string): Promise<void> {
    const transaction = await this.inner.removeCashier(cashier);
    await transaction.wait();
  }

  async getCashierNonce(cashier: string): Promise<string> {
    const nonce: string = await this.inner.getCashierNonce(cashier);
    return nonce;
  }

  async pay({
    deadline,
    nonce,
    value,
    cashierSign,
  }: PayParameters): Promise<void> {
    const transaction = await this.inner.pay(
      deadline,
      nonce,
      value,
      cashierSign,
      { value }
    );
    await transaction.wait();
  }
}

export const fetchContractStatus = async (contract: RentalAgreement) => {
  return contract.getState();
};

interface DeployProps {
  roomId: bigint;
  signer: Signer;
}

export const deployContract = async ({ roomId, signer }: DeployProps) => {
  const factory = new ContractFactory(abi, await fetchBytecode(), signer);
  const contract = await factory.deploy(roomId);
  await contract.deployTransaction.wait();
  return contract.address;
};
