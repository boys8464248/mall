// SPDX-License-Identifier: MIT
pragma solidity ^0.8.11;

import "./libs/Enumerable.sol";
import "./libs/Crypto.sol";

library Cashiers {
    using Enumerable for Enumerable.MapAddressToUint;

    struct Set {
        Enumerable.MapAddressToUint nonces;
    }

    function exists(Set storage self, address addr) internal view returns (bool) {
        return self.nonces.exists(addr);
    }

    function list(Set storage self) internal view returns (address[] storage) {
        return self.nonces.listKeys();
    }

    function getNonce(Set storage self, address cashier) internal view returns (uint) {
        return self.nonces.get(cashier);
    }

    function updateNonce(Set storage self, address cashier, bytes32 entropy) internal {
        self.nonces.put(cashier, uint(keccak256(abi.encodePacked(entropy, block.timestamp, block.number))));
    }

    function del(Set storage self, address cashier) internal {
        self.nonces.del(cashier);
    }
}


contract RentalAgreement {
    using Crypto for Crypto.Sign;
    using Cashiers for Cashiers.Set;

    string constant internal ETH_SIGNED_MSG_HEADER = "\x19\x01";
    bytes32 private myDomainHash;

    bytes32 private constant RENTAL_PERMIT_HEADER = keccak256(
        abi.encodePacked(
            "RentalPermit(",
            "uint256 deadline,",
            "address tenant,",
            "uint256 rentalRate,",
            "uint256 billingPeriodDuration,"
            "uint256 billingsCount)"
        )
    );

    bytes32 private constant TICKET_HEADER = keccak256(
        abi.encodePacked(
            "Ticket(",
            "uint256 deadline,",
            "uint256 nonce,",
            "uint256 value)"
        )
    );

    bytes32 private constant END_CONSENT_HEADER = keccak256(
        abi.encodePacked(
            "EndConsent(",
            "uint256 deadline)"
        )
    );

    address payable private landlord;
    uint private roomInternalId;
    uint private rentalRate;
    uint private billingPeriodDuration;

    address payable private tenant;

    uint private tenantProfit;
    uint private proceeds;

    uint private rentStartTime;
    uint private rentEndTime;

    bool private isRented = false;

    Cashiers.Set internal cashiers;

    enum State {READY_FOR_RENT, RENTED, ENDED}

    event PurchasePayment(uint amount);

    modifier allowState(State _state) {
        require(getState() == _state, "The contract is being in not allowed state");
        _;
    }

    modifier disallowState(State _state) {
        require(getState() != _state, "The contract is being in not allowed state");
        _;
    }

    modifier onlyLandlord() {
        require(msg.sender == landlord, "You are not a landlord");
        _;
    }

    modifier onlyTenant() {
        require(msg.sender == tenant, "You are not a tenant");
        _;
    }

    modifier validateDeadline(uint _deadline) {
        require(block.timestamp < _deadline, "The operation is outdated");
        _;
    }

    function getTenant() external view returns (address) {
        return tenant;
    }

    function getLandlord() external view returns (address) {
        return landlord;
    }

    function getRoomInternalId() external view returns (uint) {
        return roomInternalId;
    }

    function getRentalRate() external view returns (uint) { 
        return rentalRate;
    }

    function getBillingPeriodDuration() external view returns (uint) {
        return billingPeriodDuration;
    }

    function getRentStartTime() external view returns (uint) {
        return rentStartTime;
    }
    
    function getRentEndTime() external view returns (uint) {
        return rentEndTime;
    }

    constructor(uint _roomInternalId) {
        landlord = payable(msg.sender);
        roomInternalId = _roomInternalId;
        myDomainHash = keccak256(
            abi.encode(
                keccak256("EIP712Domain(string name,string version,address verifyingContract)"),
                keccak256("Rental Agreement"),
                keccak256("1.0"),
                address(this)
            )
        );
    }

    function getState() public view returns(State) {
        if (!isRented) {
            return State.READY_FOR_RENT;
        }

        if (block.timestamp > rentEndTime) {
            return State.ENDED;
        }
        
        if (getDebt() > 0) {
            return State.ENDED;
        } else {
            return State.RENTED;
        }
    }

    function rent(
        uint _deadline,
        address payable _tenant,
        uint _rentalRate,
        uint _billingPeriodDuration,
        uint _billingsCount,
        Crypto.Sign calldata _landlordSign
    ) external payable allowState(State.READY_FOR_RENT) validateDeadline(_deadline) {
        bytes32 operationHash = keccak256(
            abi.encodePacked(
                ETH_SIGNED_MSG_HEADER,
                myDomainHash,
                keccak256(
                    abi.encode(
                        RENTAL_PERMIT_HEADER,
                        _deadline,
                        _tenant,
                        _rentalRate,
                        _billingPeriodDuration,
                        _billingsCount
                    )
                )
            )
        );

        address landlordFootprint = _landlordSign.validate(operationHash);
        require(landlordFootprint == landlord, "Invalid landlord sign");
        require(_tenant == msg.sender, "The caller account and the account specified as a tenant do not match");
        require(_tenant != landlord, "The landlord cannot become a tenant");
        tenant = _tenant;

        require(_rentalRate > 0, "Rent amount should be strictly greater than zero");
        rentalRate = _rentalRate;
        require(_billingPeriodDuration > 0, "Rent period should be strictly greater than zero");
        billingPeriodDuration = _billingPeriodDuration; 
        require(_billingsCount > 0, "Rent period repeats should be strictly greater than zero");
        rentStartTime = block.timestamp;
        rentEndTime = rentStartTime + billingPeriodDuration * _billingsCount;

        require(msg.value == rentalRate, "Incorrect deposit");

        isRented = true;
    }

    function pay(
        uint _deadline,
        uint _nonce,
        uint _value,
        Crypto.Sign calldata _cashierSign
    ) external payable disallowState(State.ENDED) validateDeadline(_deadline) {
        bytes32 ticketHash = keccak256(
            abi.encodePacked(
                ETH_SIGNED_MSG_HEADER,
                myDomainHash,
                keccak256(
                    abi.encode(
                        TICKET_HEADER,
                        _deadline,
                        _nonce,
                        _value
                    )
                )
            )
        );

        address cashier = _cashierSign.validate(ticketHash);
        require(cashiers.exists(cashier), "Unknown cashier");

        assert(cashier != address(0));
        assert(getState() != State.READY_FOR_RENT); // it must be impossible to add a cashiers on ready for rent state

        require(cashiers.getNonce(cashier) == _nonce, "Invalid nonce");
        require(msg.value == _value, "Invalid value");

        cashiers.updateNonce(cashier, myDomainHash);

        uint currPeriod = getPassedPeriodsCount() + 1;
        if (currPeriod != (rentEndTime - rentStartTime) / billingPeriodDuration) // skip payment if it's last period
        {
            uint debtMonthForward = currPeriod * rentalRate;

            if (proceeds + msg.value <= debtMonthForward) {
                proceeds += msg.value;
            } else if (proceeds < debtMonthForward) {
                uint payment = debtMonthForward - proceeds;
                proceeds += payment;
                tenantProfit += msg.value - payment;
            } else {
                tenantProfit += msg.value;
            }
        }
        else {
            tenantProfit += msg.value;
        }

        emit PurchasePayment(msg.value);
    }

    function getPassedPeriodsCount() internal view returns (uint) {
        uint ppc = (block.timestamp - rentStartTime) / billingPeriodDuration;
        uint periods_count = (rentEndTime - rentStartTime) / billingPeriodDuration;
        if (ppc > periods_count)
            return periods_count;
        else
            return ppc;
    }

    function getExpectedProceeds() internal view returns (uint) {
        uint ppc = getPassedPeriodsCount();
        uint pps = proceeds / rentalRate;

        if (block.timestamp > rentEndTime)
            ppc--;
        
        if (pps >= ppc)
            return ppc * rentalRate;
        else
            return (pps + 1) * rentalRate;
        
        // Note: 0 <= expectedProceeds - proceeds <= rentalRate
    }

    function getDebt() internal view returns (uint) {
        uint expectedProceeds = getExpectedProceeds();
        if (expectedProceeds > proceeds)
            return expectedProceeds - proceeds;
        else
            return 0;
    }

    function getMonthForwardOverpayment() internal view returns (uint) {
        uint expectedProceeds = getExpectedProceeds();
        if (expectedProceeds < proceeds) {
            assert(rentalRate >= proceeds - expectedProceeds);
            return proceeds - expectedProceeds;
        }
        else
            return 0;
    }

    function transfer(address addr, uint value) internal {
        (bool success, ) = addr.call{value:value}("");
        require(success, "Transfer failed");
    }

    function getTenantProfit() public view returns (uint) {
        return tenantProfit;
    }

    function withdrawTenantProfit() external onlyTenant {
        transfer(tenant, getTenantProfit());
        tenantProfit = 0;
    }

    function getLandlordProfit() public view returns (uint) {
        assert(address(this).balance >= tenantProfit + getMonthForwardOverpayment());
        return address(this).balance - tenantProfit - getMonthForwardOverpayment();
    }

    function withdrawLandlordProfit() external onlyLandlord {
        transfer(landlord, getLandlordProfit());
    }

    function addCashier(address _addr) external onlyTenant {
        require(_addr != address(0), "Zero address cannot become a cashier");
        require(_addr != landlord, "The landlord cannot become a cashier");
        cashiers.updateNonce(_addr, myDomainHash);
    }

    function removeCashier(address _addr) external onlyTenant {
        require(cashiers.exists(_addr), "Unknown cashier");
        cashiers.del(_addr);
    }

    function getCashierNonce(address _addr) external view returns (uint) {
        if (cashiers.exists(_addr))
            return cashiers.getNonce(_addr);
        else
            return 0;
    }

    function getCashiersList() external view returns (address[] memory) {
        return cashiers.list();
    }

    function endAgreementManually(
        uint _deadline,
        Crypto.Sign calldata _landlordSign,
        Crypto.Sign calldata _tenantSign
    ) external allowState(State.RENTED) validateDeadline(_deadline) {
        bytes32 operationHash = keccak256(
            abi.encodePacked(
                ETH_SIGNED_MSG_HEADER,
                myDomainHash,
                keccak256(
                    abi.encode(
                        END_CONSENT_HEADER,
                        _deadline
                    )
                )
            )
        );

        address landlordFootprint = _landlordSign.validate(operationHash);
        require(landlordFootprint == landlord, "Invalid landlord sign");

        address tenantFootprint = _tenantSign.validate(operationHash);
        require(tenantFootprint == tenant, "Invalid tenant sign");

        transfer(tenant, tenantProfit + getMonthForwardOverpayment());
        selfdestruct(landlord);
    }

    function endAgreement() external allowState(State.ENDED) {
        uint tenant_debt = getDebt();
        assert(rentalRate > tenant_debt);
        if (tenant_debt > 0) {
            // return tenant's overpayment from the last month too (rentalRate - tenant_debt)
            transfer(tenant, tenantProfit + rentalRate - tenant_debt);
        }
        else {
            transfer(tenant, tenantProfit + getMonthForwardOverpayment());
        }
        selfdestruct(landlord);
    }
}
