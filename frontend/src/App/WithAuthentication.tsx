import { providers, Signer } from "ethers";
import { Navigate, Route, Routes } from "react-router";
import { useEffect, useState } from "react";

import useAuthentication from "./useAuthentication";
import Authentication from "../Authentication";
import RoomsList from "../rooms/RoomsList";
import { CreateRoom } from "../rooms/CreateRoom";
import RoomRouter from "../rooms/RoomRouter";

interface Props {
  provider: providers.Web3Provider;
}

const WithAuthentication = ({ provider }: Props) => {
  const authentication = useAuthentication({ provider });

  const [signer, setSigner] = useState<Signer | null>(null);
  useEffect(() => {
    if (authentication.status === `authenticated`) {
      if (signer === null) {
        setSigner(provider.getSigner(authentication.address));
      }
    } else if (signer !== null) {
      setSigner(null);
    }
  }, [authentication, provider, signer]);

  if (authentication.status === `loading`) {
    return null;
  }

  if (authentication.status !== `authenticated`) {
    return (
      <Authentication
        connect={authentication.connect}
        provider={provider}
        status={authentication.status}
        onAuthenticated={authentication.onAuthenticated}
      />
    );
  }

  if (!signer) {
    return null;
  }

  return (
    <div className="app">
      <div className="account">
        Authenticated as {}
        <span className="account__address">{authentication.address}</span>
      </div>

      <Routes>
        <Route
          path="/rooms"
          element={
            <RoomsList
              isLandlord={authentication.isLandlord}
              provider={provider}
            />
          }
        />
        <Route path="/rooms/create" element={<CreateRoom />} />
        <Route
          path="/room/:id/*"
          element={
            <RoomRouter
              authentication={authentication}
              provider={provider}
              signer={signer}
            />
          }
        />
        <Route path="/" element={<Navigate to="/rooms" />} />
      </Routes>
    </div>
  );
};

export default WithAuthentication;
