import { gql, useQuery } from "@apollo/client";
import { providers } from "ethers";
import { useState } from "react";

import { Authentication } from "../types";
import useConnect from "../utils/useConnect";

const QUERY = gql`
  query {
    authentication {
      address
      isLandlord
    }
  }
`;

interface Result {
  authentication: Authentication | null;
}

type AuthenticationStatus =
  | { status: `loading` }
  | ({ status: `authenticated` } & Authentication)
  | {
      status: `unauthenticated` | `outdated`;
      connect(): Promise<string>;
      onAuthenticated(authentication: Authentication): void;
    };

interface Props {
  provider: providers.Web3Provider;
}

const useAuthentication = ({ provider }: Props): AuthenticationStatus => {
  const { account, connect } = useConnect({ provider });
  const [authentication, setAuthentication] = useState<Authentication | null>();

  const { loading } = useQuery<Result>(QUERY, {
    onCompleted(data) {
      setAuthentication(data.authentication);
    },
  });
  if (loading) {
    return { status: `loading` };
  }

  if (!authentication) {
    return {
      status: `unauthenticated`,
      onAuthenticated: setAuthentication,
      connect,
    };
  }

  if (account !== authentication.address) {
    return { status: `outdated`, onAuthenticated: setAuthentication, connect };
  }

  return { status: `authenticated`, ...authentication };
};

export default useAuthentication;
