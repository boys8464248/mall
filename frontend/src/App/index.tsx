import "./style.css";

import { Route, Routes } from "react-router";
import { useState } from "react";
import { providers } from "ethers";

import WithAuthentication from "./WithAuthentication";
import Ticket from "../Ticket";

const App = () => {
  const [provider] = useState(
    () => new providers.Web3Provider(window.ethereum)
  );

  return (
    <Routes>
      <Route path="/ticket/:id" element={<Ticket provider={provider} />} />
      <Route path="*" element={<WithAuthentication provider={provider} />} />
    </Routes>
  );
};

export default App;
