import "./style.css";

import { providers } from "ethers";
import { useParams } from "react-router";

import useConnect from "../utils/useConnect";
import useFetchTicket from "./useFetchTicket";
import { useCallback, useMemo, useState } from "react";
import { isBefore } from "date-fns";
import { RentalAgreement } from "../contract";

interface Props {
  provider: providers.Web3Provider;
}

const Ticket = ({ provider }: Props) => {
  const ticketId = useParams().id!;
  const ticket = useFetchTicket({ ticketId });
  const deadline = useMemo(
    () => (ticket ? new Date(ticket.deadline.datetime) : null),
    [ticket]
  );

  const { account, connect } = useConnect({ provider });
  const signer = useMemo(
    () => (account ? provider.getSigner(account) : null),
    [account, provider]
  );

  const [hasPaid, setHasPaid] = useState(false);
  const handlePay = useCallback(async () => {
    if (!ticket || !signer) {
      return;
    }
    const contract = new RentalAgreement({
      address: ticket.room.contractAddress!,
      signer,
    });
    const cashierSign = {
      ...ticket.cashierSignature,
      v: Number(ticket.cashierSignature.v),
    };

    await contract.pay({ deadline: Number(deadline) / 1000, nonce: BigInt(ticket.nonce.value), value: BigInt(ticket.value.wei), cashierSign });
    setHasPaid(true);
  }, [deadline, signer, ticket]);

  if (!ticket || !deadline) {
    return null;
  }

  const isDeadlineAhead = isBefore(Date.now(), deadline);

  const accountElement = (
    <div className="account">
      {account ? (
        <>
          Connected to {}
          <span className="account__address">{account}</span>
        </>
      ) : (
        `Disconnected`
      )}
    </div>
  );

  if (hasPaid) {
    return (
      <div className="app">
        {accountElement}

        <h1 className="ticket__success">Payment successful</h1>
      </div>
    );
  }

  return (
    <div className="app app--ticket">
      {accountElement}

      <main className="ticket">
        <p className="ticket__info">
          <span className="ticket__store">
            {ticket.room.publicName ?? ticket.room.internalName}
          </span>
          {} {isDeadlineAhead ? `wants` : `wanted`} you to pay {}
          <span className="ticket__value">{ticket.value.wei} wei</span> {}
          before {}
          <span className="ticket__deadline">{deadline.toUTCString()}</span>
        </p>
        {isDeadlineAhead &&
          (account ? (
            <button
              type="button"
              className="button button--big ticket__pay"
              onClick={handlePay}
            >
              Pay
            </button>
          ) : (
            <button
              type="button"
              className="button button--big account__connect"
              onClick={connect}
            >
              Connect to MetaMask
            </button>
          ))}
        {!isDeadlineAhead && (
          <p className="ticket__past-deadline">
            Current time is past the deadline
          </p>
        )}
      </main>
    </div>
  );
};

export default Ticket;
