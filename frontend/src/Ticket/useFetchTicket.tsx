import { gql, useQuery } from "@apollo/client";
import { Room, Signature } from "../types";

const FETCH_TICKET = gql`
  query ($ticketId: ID!) {
    ticket(id: $ticketId) {
      room {
        internalName
        publicName
        contractAddress
      }
      nonce {
        value
      }
      value {
        wei
      }
      deadline {
        datetime
      }
      cashierSignature {
        v
        r
        s
      }
    }
  }
`;

interface Variables {
  ticketId: string;
}

interface Result {
  ticket: {
    room: Pick<Room, "internalName" | "publicName" | "contractAddress">;
    nonce: { value: string };
    value: { wei: string };
    deadline: { datetime: string };
    cashierSignature: Signature;
  };
}

interface Props {
  ticketId: string;
}

const useFetchTicket = ({ ticketId }: Props) => {
  const { data } = useQuery<Result, Variables>(FETCH_TICKET, {
    variables: { ticketId },
  });
  return data?.ticket;
};

export default useFetchTicket;
