import { utils } from "ethers";
import { Duration } from "date-fns";
import { RoomStatus } from "../types";

export const normalizeAddresses = (addresses: string[]): string[] =>
  addresses.map((address) => utils.getAddress(address));

const ROOM_STATUSES = {
  rentUnavailable: `Unavailable for renting`,
  rentAvailable: `Available for renting`,
  rented: `Rented`,
  rentEnded: `Rent ended`,
};
export const displayRoomStatus = (status: RoomStatus) => ROOM_STATUSES[status];

export const billingPeriodToDuration = (period: number): Duration => {
  const seconds = period % 60;
  period = Math.floor(period / 60);

  const minutes = period % 60;
  period = Math.floor(period / 60);

  const hours = period % 24;
  period = Math.floor(period / 24);

  const days = period;

  return { days, hours, minutes, seconds };
};

export const makeDomain = (verifyingContract: string) => ({
  name: `Rental Agreement`,
  version: `1.0`,
  verifyingContract,
});
