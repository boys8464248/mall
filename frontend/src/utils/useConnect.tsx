import { providers, utils } from "ethers";
import { useCallback, useEffect, useState } from "react";
import { normalizeAddresses } from ".";

interface Props {
  provider: providers.Web3Provider;
}

interface Connection {
  account?: string;
  connect(): Promise<string>;
}

const useConnect = ({ provider }: Props): Connection => {
  const [accounts, setAccounts] = useState<string[] | null>(null);
  useEffect(() => {
    let isCancelled = false;

    provider.listAccounts().then((accounts) => {
      if (!isCancelled) {
        setAccounts(normalizeAddresses(accounts));
      }
    });

    return () => {
      isCancelled = true;
    };
  }, [provider]);

  useEffect(() => {
    const handler = (accounts: string[]) =>
      setAccounts(normalizeAddresses(accounts));
    window.ethereum.on(`accountsChanged`, handler);

    return () => {
      window.ethereum.removeListener(`accountsChanged`, handler);
    };
  }, []);

  const connect = useCallback(async () => {
    if (accounts && accounts.length > 0) {
      return accounts[0];
    }

    const [rawAddress]: [string] = await provider.send(
      "eth_requestAccounts",
      []
    );
    const address = utils.getAddress(rawAddress);

    return address;
  }, [accounts, provider]);

  if (accounts) {
    return { account: accounts[0], connect };
  }

  return { connect };
};

export default useConnect;
