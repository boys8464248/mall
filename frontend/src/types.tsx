import { BigNumber } from "ethers";

export interface Authentication {
  address: string;
  isLandlord: boolean;
}

export interface Signature {
  r: string;
  s: string;
  v: string;
}

export interface Room {
  id: string;
  internalName: string;
  area: number;
  location: string;

  contractAddress: string | null;
  publicName: string | null;
}

export type RoomStatus =
  | `rentUnavailable`
  | `rentAvailable`
  | `rented`
  | `rentEnded`;

type WithRoomStatus =
  | { status: `rentAvailable` | `rentUnavailable` }
  | {
      status: Exclude<RoomStatus, `rentAvailable` | `rentUnavailable`>;
      tenant: string;
      rentStart: Date;
      rentEnd: Date;
      billingPeriod: number;
      rentalRate: BigNumber;
    };

export type RoomInfo = Room & WithRoomStatus;

export interface InputRoom {
  internalName: string;
  area: number;
  location: string;
}

interface ContractSignature {
  r: string;
  s: string;
  v: number;
}

export interface RentParameters {
  deadline: number;
  tenant: string;
  rentalRate: string;
  billingPeriodDuration: number;
  billingsCount: number;
  sign: ContractSignature;
}

export interface PayParameters {
  deadline: number;
  nonce: bigint;
  value: bigint;
  cashierSign: ContractSignature;
}
