import "./style.css";

import { ReactNode } from "react";

interface Props {
  title: ReactNode;
  children?: ReactNode;
}

const Header = ({ title, children }: Props) => {
  return (
    <header className="header">
      {typeof title === `string` ? (
        <h1 className="header__title">{title}</h1>
      ) : (
        <div className="header__complex-title">{title}</div>
      )}
      {children && <div className="header__controls">{children}</div>}
    </header>
  );
};

export default Header;
