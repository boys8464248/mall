import { useCallback, useEffect, useState } from "react";
import { useNavigate } from "react-router";
import isEqual from "lodash/isEqual";

import useEditRoom from "./useEditRoom";
import RoomForm, { PartialRoom } from "../RoomForm";
import { handleError } from "../../errors";
import Header from "../../Header";
import { InputRoom, RoomInfo } from "../../types";

interface Props {
  room: RoomInfo;
  onRoomChange(room: RoomInfo): void;
}

const makeEditedRoom = ({
  internalName,
  area,
  location,
}: RoomInfo): PartialRoom => ({
  internalName,
  area,
  location,
});

const EditRoom = ({ room, onRoomChange }: Props) => {
  const navigate = useNavigate();

  const [editedRoom, setEditedRoom] = useState<PartialRoom>(() =>
    makeEditedRoom(room)
  );
  useEffect(() => {
    setEditedRoom((current) => {
      const newEditedRoom = makeEditedRoom(room);
      if (isEqual(current, newEditedRoom)) {
        return current;
      }
      return newEditedRoom;
    });
  }, [room]);

  const editRoom = useEditRoom({ roomId: room.id });
  const handleSubmit = useCallback(
    async (editedRoom: InputRoom) => {
      try {
        await editRoom(editedRoom);
        onRoomChange({
          ...room,
          ...editedRoom,
        });
        navigate(`/room/${room.id}`);
      } catch (error) {
        handleError(error);
      }
    },
    [editRoom, navigate, onRoomChange, room]
  );

  if (!room) {
    return null;
  }

  return (
    <>
      <Header title="Edit the room" />
      <RoomForm
        room={editedRoom}
        onChange={setEditedRoom}
        onSubmit={handleSubmit}
        submitText="Save"
      />
    </>
  );
};

export default EditRoom;
