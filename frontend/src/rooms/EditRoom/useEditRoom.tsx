import { gql, useMutation } from "@apollo/client";
import { useCallback } from "react";
import { InputRoom } from "../../types";

const EDIT_ROOM = gql`
  mutation ($roomId: ID!, $room: InputRoom!) {
    room: editRoom(id: $roomId, room: $room) {
      id
    }
  }
`;

interface Variables {
  roomId: string;
  room: InputRoom;
}

interface Props {
  roomId: string;
}

const useEditRoom = ({ roomId }: Props) => {
  const [editRoom] = useMutation < {}, Variables>(EDIT_ROOM);

  return useCallback(async (room: InputRoom) => {
    const { errors } = await editRoom({ variables: { roomId, room }
    });
    if (errors) {
      throw errors[0];
    }
  }, [editRoom, roomId]);
};

export default useEditRoom;
