import { gql, useMutation } from "@apollo/client";

const REMOVE_ROOM = gql`
  mutation ($roomId: ID!) {
    removeRoom(id: $roomId) {
      id
    }
  }
`;

interface Variables {
  roomId: string;
}

interface Props {
  roomId: string;
}

const useRemoveRoom = ({ roomId }: Props) => {
  const [removeRoom] = useMutation<{}, Variables>(REMOVE_ROOM, {
    variables: { roomId },
  });

  return removeRoom;
};

export default useRemoveRoom;
