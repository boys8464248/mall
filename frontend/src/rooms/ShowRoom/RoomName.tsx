import { useCallback, useEffect, useState } from "react";
import { RoomInfo } from "../../types";

interface Props {
  room: RoomInfo;
  isEditing: boolean;
  onChange(newName: string | null): void;
}

const RoomName = ({ room, isEditing, onChange }: Props) => {
  const [editedName, setEditedName] = useState(room.publicName ?? ``);
  useEffect(() => {
    setEditedName(room.publicName ?? ``);
  }, [room.publicName]);

  const handleSubmit = useCallback(
    (event: React.FormEvent) => {
      event.preventDefault();
      onChange(editedName === `` ? null : editedName);
    },
    [editedName, onChange]
  );

  return (
    <>
      {isEditing ? (
        <form className="public-name-edit" onSubmit={handleSubmit}>
          <input
            className="input public-name-edit__name"
            type="text"
            value={editedName}
            onChange={(event) => setEditedName(event.currentTarget.value)}
          />
          <button className="button public-name-edit__submit">Save</button>
        </form>
      ) : (
        <h1 className="header__title room__name">
          {room.publicName ?? room.internalName}
        </h1>
      )}
      {(isEditing || room.publicName !== null) && (
        <p className="room__internal-name">{room.internalName}</p>
      )}
    </>
  );
};

export default RoomName;
