import { gql, useMutation } from "@apollo/client";
import { useCallback } from "react";

const SET_CONTRACT_ADDRESS = gql`
  mutation ($roomId: ID!, $contractAddress: String!) {
    setRoomContractAddress(id: $roomId, contractAddress: $contractAddress) {
      id
    }
  }
`;

interface Variables {
  roomId: string;
  contractAddress: string;
}

interface Props {
  roomId: string;
}

const useSetContractAddress = ({ roomId }: Props) => {
  const [setContractAddress] = useMutation<{}, Variables>(SET_CONTRACT_ADDRESS);

  return useCallback(
    async (contractAddress: string) => {
      const { errors } = await setContractAddress({
        variables: { roomId, contractAddress },
      });
      if (errors) {
        throw errors[0];
      }
    },
    [roomId, setContractAddress]
  );
};

export default useSetContractAddress;
