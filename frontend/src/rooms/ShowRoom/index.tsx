import "./style.css";

import { Signer } from "ethers";
import { useNavigate } from "react-router";
import { useCallback, useState } from "react";
import { Link } from "react-router-dom";
import { formatDuration } from "date-fns";

import useRemoveRoom from "./useRemoveRoom";
import useSetContractAddress from "./useSetContractAddress";
import Header from "../../Header";
import { billingPeriodToDuration, displayRoomStatus } from "../../utils";
import { handleError } from "../../errors";
import { deployContract } from "../../contract";
import { Authentication, RoomInfo } from "../../types";
import RoomName from "./RoomName";
import useSetPublicName from "./useSetPublicName";

interface Props {
  authentication: Authentication;
  signer: Signer;
  room: RoomInfo;
  onRoomChange(room: RoomInfo): void;
}

const ShowRoom = ({ authentication, signer, room, onRoomChange }: Props) => {
  const navigate = useNavigate();

  const removeRoom = useRemoveRoom({ roomId: room.id });
  const handleRemoveRoom = useCallback(async () => {
    try {
      await removeRoom();
      navigate(`/rooms`);
    } catch (error) {
      handleError(error);
    }
  }, [navigate, removeRoom]);

  const setContractAddress = useSetContractAddress({ roomId: room.id });
  const handleAllowRenting = useCallback(async () => {
    if (!room) {
      return;
    }

    const contractAddress = await deployContract({
      roomId: BigInt(room.id),
      signer,
    });
    await setContractAddress(contractAddress);
    onRoomChange({
      ...room,
      status: `rentAvailable`,
      contractAddress,
    });
  }, [onRoomChange, room, setContractAddress, signer]);

  const [isEditingName, setIsEditingName] = useState(false);
  const setPublicName = useSetPublicName({ roomId: room.id });
  const handleNewName = useCallback(
    async (name: string | null) => {
      await setPublicName(name);
      setIsEditingName(false);
      onRoomChange({
        ...room,
        publicName: name,
      });
    },
    [onRoomChange, room, setPublicName]
  );

  if (!room) {
    return null;
  }

  return (
    <>
      <Header
        title={
          <RoomName
            room={room}
            isEditing={isEditingName}
            onChange={handleNewName}
          />
        }
      >
        {authentication.isLandlord && (
          <>
            <Link to="./edit" className="button room__edit">
              Edit the room
            </Link>
            {room.contractAddress === null && (
              <button
                type="button"
                className="button button--dangerous room__remove"
                onClick={handleRemoveRoom}
              >
                Remove the room
              </button>
            )}
          </>
        )}
        {`tenant` in room && room.tenant === authentication.address && (
          <>
            {!isEditingName && (
              <button
                type="button"
                className="button room__edit-public-name"
                onClick={() => setIsEditingName(true)}
              >
                Edit public name
              </button>
            )}
            <Link to="./cashiers" className="button room__manage-cashiers">
              Manage cashiers
            </Link>
          </>
        )}
      </Header>
      <main className="room">
        <dl className="room__info">
          <dt className="room__label">Status</dt>
          <dd className="room__value">
            <p className="room__status">{displayRoomStatus(room.status)}</p>
            {room.status === `rentUnavailable` && (
              <button
                type="button"
                className="button room__allow-renting"
                onClick={handleAllowRenting}
              >
                Allow renting
              </button>
            )}
            {room.status === `rentAvailable` &&
              (authentication.isLandlord ? (
                <Link to="./sign" className="button">
                  Create a rental permit
                </Link>
              ) : (
                <Link to="./rent" className="button">
                  Rent
                </Link>
              ))}
          </dd>

          <dt className="room__label">Area</dt>
          <dd className="room__value room__area">{room.area} sq.m.</dd>

          <dt className="room__label">Location</dt>
          <dd className="room__value room__location">{room.location}</dd>

          {room.contractAddress && (
            <>
              <dt className="room__label">Contract address</dt>
              <dd className="room__value room__contract-address">
                {room.contractAddress}
              </dd>
            </>
          )}

          {(room.status === `rented` || room.status === `rentEnded`) && (
            <>
              <dt className="room__label">Tenant</dt>
              <dd className="room__value room__tenant">{room.tenant}</dd>

              <dt className="room__label">Rent start time</dt>
              <dd className="room__value room__rent-start">
                {room.rentStart.toUTCString()}
              </dd>

              <dt className="room__label">Rent end time</dt>
              <dd className="room__value room__rent-end">
                {room.rentEnd.toUTCString()}
              </dd>

              <dt className="room__label">Billing period</dt>
              <dd className="room__value room__billing-period">
                {formatDuration(billingPeriodToDuration(room.billingPeriod))}
              </dd>

              <dt className="room__label">Rental rate</dt>
              <dd className="room__value room__rental-rate">
                {room.rentalRate.toString()} wei
              </dd>
            </>
          )}
        </dl>
      </main>
    </>
  );
};

export default ShowRoom;
