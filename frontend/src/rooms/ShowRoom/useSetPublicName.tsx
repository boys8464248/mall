import { gql, useMutation } from "@apollo/client";
import { useCallback } from "react";

const SET_PUBLIC_NAME = gql`
  mutation ($roomId: ID!, $name: String) {
    setRoomPublicName(id: $roomId, publicName: $name) {
      id
    }
  }
`;

interface Variables {
  roomId: string;
  name: string | null;
}

interface Props {
  roomId: string;
}

const useSetPublicName = ({ roomId }: Props) => {
  const [setPublicName] = useMutation<{}, Variables>(SET_PUBLIC_NAME);

  return useCallback(
    async (name: string | null) => {
      const { errors } = await setPublicName({ variables: { roomId, name } });
      if (errors) {
        throw errors[0];
      }
    },
    [roomId, setPublicName]
  );
};

export default useSetPublicName;
