import { gql, useQuery } from "@apollo/client";
import { providers } from "ethers";
import { useState } from "react";

import { RentalAgreement } from "../../contract";
import { Room, RoomStatus } from "../../types";

const FETCH_ROOMS = gql`
  query {
    rooms {
      id
      internalName
      publicName
      contractAddress
    }
  }
`;

type PartialRoom = Pick<
  Room,
  "id" | "internalName" | "publicName" | "contractAddress"
>;
type RoomCard = PartialRoom & { status: RoomStatus };

interface Result {
  rooms: PartialRoom[];
}

interface Props {
  provider: providers.Web3Provider;
}

const useFetchRooms = ({ provider }: Props) => {
  const [rooms, setRooms] = useState<RoomCard[] | null>(null);
  useQuery<Result, {}>(FETCH_ROOMS, {
    async onCompleted({ rooms }) {
      const roomsWithStatus = await Promise.all(
        rooms.map(async (room) => {
          if (room.contractAddress === null) {
            return { ...room, status: `rentUnavailable` as const };
          }

          const contract = new RentalAgreement({
            address: room.contractAddress,
            provider,
          });
          const status = await contract.getState();
          return { ...room, status };
        })
      );

      setRooms(roomsWithStatus);
    },
  });

  return rooms;
};

export default useFetchRooms;
