import "./style.css";

import { Link } from "react-router-dom";
import { providers } from "ethers";

import useFetchRooms from "./useFetchRooms";
import Header from "../../Header";
import { displayRoomStatus } from "../../utils";

interface Props {
  isLandlord: boolean;
  provider: providers.Web3Provider;
}

const RoomsList = ({ isLandlord, provider }: Props) => {
  const rooms = useFetchRooms({ provider });

  return (
    <>
      <Header title="Rooms">
        {isLandlord && (
          <Link to="/rooms/create" className="button rooms__create">
            Create a room
          </Link>
        )}
      </Header>
      <main className="rooms">
        {rooms &&
          rooms.map((room) => (
            <div className="room-card" id={`room-${room.id}`} key={room.id}>
              <h2 className="room-card__name">
                {room.publicName ?? room.internalName}
              </h2>
              <p className="room-card__status">
                {displayRoomStatus(room.status)}
              </p>
              <Link
                to={`/room/${room.id}`}
                className="button room-card__details"
              >
                View more
              </Link>
            </div>
          ))}
      </main>
    </>
  );
};

export default RoomsList;
