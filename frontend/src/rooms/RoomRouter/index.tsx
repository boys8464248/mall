import { providers, Signer } from "ethers";
import { Route, Routes, useParams } from "react-router";

import { Authentication } from "../../types";
import Cashiers from "../Cashiers";
import CreateRentalPermit from "../CreateRentalPermit";
import CreateTicket from "../CreateTicket";
import EditRoom from "../EditRoom";
import RentRoom from "../RentRoom";
import ShowRoom from "../ShowRoom";
import useFetchRoom from "./useFetchRoom";

interface Props {
  authentication: Authentication;
  provider: providers.Web3Provider;
  signer: Signer;
}

const RoomRouter = ({ authentication, provider, signer }: Props) => {
  const roomId = useParams().id!;
  const { room, setRoom, refetchRoom } = useFetchRoom({ provider, roomId });

  if (!room) {
    return null;
  }

  return (
    <Routes>
      <Route
        path="edit"
        element={<EditRoom room={room} onRoomChange={setRoom} />}
      />
      <Route
        path="sign"
        element={<CreateRentalPermit signer={signer} room={room} />}
      />
      <Route
        path="rent"
        element={
          <RentRoom room={room} signer={signer} refetchRoom={refetchRoom} />
        }
      />
      <Route
        path="cashiers"
        element={<Cashiers room={room} signer={signer} />}
      />
      <Route
        path="create-ticket"
        element={
          <CreateTicket
            authentication={authentication}
            room={room}
            signer={signer}
          />
        }
      />
      <Route
        path="/"
        element={
          <ShowRoom
            authentication={authentication}
            signer={signer}
            room={room}
            onRoomChange={setRoom}
          />
        }
      />
    </Routes>
  );
};

export default RoomRouter;
