import { gql, useQuery } from "@apollo/client";
import { providers } from "ethers";
import { useCallback, useState } from "react";

import { RentalAgreement } from "../../contract";
import { Room, RoomInfo } from "../../types";

const FETCH_ROOM = gql`
  query ($roomId: ID!) {
    room(id: $roomId) {
      id
      internalName
      area
      location
      publicName
      contractAddress
    }
  }
`;

interface Variables {
  roomId: string;
}

interface Result {
  room: Room;
}

interface Props {
  provider: providers.Web3Provider;
  roomId: string;
}

const useFetchRoom = ({ provider, roomId }: Props) => {
  const [room, setRoom] = useState<RoomInfo | null>(null);
  const { refetch } = useQuery<Result, Variables>(FETCH_ROOM, {
    variables: { roomId },
    async onCompleted({ room }) {
      if (room.contractAddress === null) {
        setRoom({ ...room, status: `rentUnavailable` });
        return;
      }

      const contract = new RentalAgreement({
        address: room.contractAddress,
        provider,
      });
      const status = await contract.getState();
      if (status === `rentAvailable`) {
        setRoom({ ...room, status });
        return;
      }

      setRoom({
        ...room,
        status,
        tenant: await contract.getTenant(),
        rentStart: await contract.getRentStartTime(),
        rentEnd: await contract.getRentEndTime(),
        billingPeriod: await contract.getBillingPeriodDuration(),
        rentalRate: await contract.getRentalRate(),
      });
    },
  });

  const refetchRoom = useCallback(async () => {
    await refetch();
  }, [refetch]);

  return { room, setRoom, refetchRoom };
};

export default useFetchRoom;
