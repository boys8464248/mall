import { Signer, utils } from "ethers";
import { useCallback, useMemo, useState } from "react";
import Header from "../../Header";
import { RentParameters, RoomInfo } from "../../types";
import { useNavigate } from "react-router";
import { makeDomain } from "../../utils";

interface RentalPermit {
  deadline: number;
  tenant: string;
  rentalRate: bigint;
  billingPeriodDuration: number;
  billingsCount: number;
}

type EditedRentalPermit = {
  deadline: string;
  tenant: string;
  rentalRate: bigint | null;
  billingPeriodDuration: number | null;
  billingsCount: number | null;
};

const TYPES = {
  RentalPermit: [
    { name: `deadline`, type: `uint256` },
    { name: `tenant`, type: `address` },
    { name: `rentalRate`, type: `uint256` },
    { name: `billingPeriodDuration`, type: `uint256` },
    { name: `billingsCount`, type: `uint256` },
  ],
};

interface Props {
  signer: Signer;
  room: RoomInfo;
}

const download = (name: string, content: string) => {
  const blob = URL.createObjectURL(new File([content], name));
  const link = document.createElement(`a`);
  link.href = blob;
  link.download = name;

  document.body.append(link);
  link.click();
  link.remove();
};

const CreateRentalPermit = ({ signer, room }: Props) => {
  const navigate = useNavigate();
  const domain = useMemo(
    () => makeDomain(room.contractAddress!),
    [room.contractAddress]
  );

  const [editedPermit, setEditedPermit] = useState<EditedRentalPermit>({
    deadline: ``,
    tenant: ``,
    rentalRate: null,
    billingPeriodDuration: null,
    billingsCount: null,
  });

  const handleSubmit = useCallback(
    async (event: React.FormEvent) => {
      event.preventDefault();

      const permit: RentalPermit = {
        deadline: Number(new Date(editedPermit.deadline)) / 1000,
        tenant: editedPermit.tenant,
        rentalRate: editedPermit.rentalRate!,
        billingPeriodDuration: editedPermit.billingPeriodDuration!,
        billingsCount: editedPermit.billingsCount!,
      };

      // @ts-expect-error: `_signTypedData` is experimental, but it exists
      const rawSignature = await signer._signTypedData(domain, TYPES, permit);
      const { r, s, v } = utils.splitSignature(rawSignature);

      const rentParameters: RentParameters = {
        ...permit,
        rentalRate: String(permit.rentalRate),
        sign: { r, s, v },
      };
      download(`landlordSign.json`, JSON.stringify(rentParameters));
      navigate(-1);
    },
    [editedPermit, domain, signer, navigate]
  );

  return (
    <>
      <Header title="Create a rental permit" />
      <form className="form" onSubmit={handleSubmit}>
        <label htmlFor="deadline" className="form__label">
          Deadline
        </label>
        <div className="form__control">
          <input
            className="input"
            type="datetime-local"
            id="deadline"
            value={editedPermit.deadline}
            onChange={(event) =>
              setEditedPermit({
                ...editedPermit,
                deadline: event.currentTarget.value,
              })
            }
            required={true}
          />
        </div>

        <label htmlFor="tenant" className="form__label">
          Tenant address
        </label>
        <div className="form__control">
          <input
            className="input"
            type="text"
            id="tenant"
            value={editedPermit.tenant ?? ``}
            onChange={(event) =>
              setEditedPermit({
                ...editedPermit,
                tenant: event.currentTarget.value,
              })
            }
            required={true}
            minLength={42}
            maxLength={42}
          />
        </div>

        <label htmlFor="rental-rate" className="form__label">
          Rental rate
        </label>
        <div className="form__control">
          <input
            className="input"
            type="text"
            id="rental-rate"
            value={String(editedPermit.rentalRate ?? ``)}
            onChange={(event) =>
              setEditedPermit({
                ...editedPermit,
                rentalRate: BigInt(event.currentTarget.value),
              })
            }
            required={true}
          />
          wei
        </div>

        <label htmlFor="billing-period" className="form__label">
          Billing period
        </label>
        <div className="form__control">
          <input
            className="input"
            type="number"
            id="billing-period"
            value={editedPermit.billingPeriodDuration ?? ``}
            onChange={(event) =>
              setEditedPermit({
                ...editedPermit,
                billingPeriodDuration: event.currentTarget.valueAsNumber,
              })
            }
            required={true}
            min={0}
          />
          seconds
        </div>

        <label htmlFor="billings-count" className="form__label">
          Billings count
        </label>
        <div className="form__control">
          <input
            className="input"
            type="number"
            id="billings-count"
            value={editedPermit.billingsCount ?? ``}
            onChange={(event) =>
              setEditedPermit({
                ...editedPermit,
                billingsCount: event.currentTarget.valueAsNumber,
              })
            }
            required={true}
            min={0}
          />
          times
        </div>

        <button type="submit" className="button form__submit">
          Sign and download
        </button>
      </form>
    </>
  );
};

export default CreateRentalPermit;
