import { gql, useMutation } from "@apollo/client";
import { useCallback } from "react";
import { Signature } from "../../types";

const CREATE_TICKET = gql`
  mutation ($ticket: InputTicket!) {
    ticket: createTicket(ticket: $ticket) {
      id
    }
  }
`;

interface InputTicket {
  room: string;
  nonce: { value: string };
  value: { wei: string };
  deadline: { datetime: string };
  cashierSignature: Signature;
}

interface Variables {
  ticket: InputTicket;
}

interface Result {
  ticket: { id: string };
}

interface Props {
  roomId: string;
}

const useCreateTicket = ({ roomId }: Props) => {
  const [createTicket] = useMutation<Result, Variables>(CREATE_TICKET);

  interface CallbackProps {
    nonce: bigint;
    value: bigint;
    deadline: Date;
    signature: Signature;
  }
  return useCallback(
    async ({ nonce, value, deadline, signature }: CallbackProps) => {
      const { data, errors } = await createTicket({
        variables: {
          ticket: {
            room: roomId,
            nonce: { value: String(nonce) },
            value: { wei: String(value) },
            deadline: { datetime: deadline.toISOString() },
            cashierSignature: signature,
          },
        },
      });
      if (errors) {
        throw errors[0];
      }

      return data!.ticket.id;
    },
    [createTicket, roomId]
  );
};

export default useCreateTicket;
