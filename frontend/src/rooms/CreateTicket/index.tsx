import { Signer, utils } from "ethers";
import { FormEvent, useCallback, useEffect, useMemo, useState } from "react";
import { useNavigate } from "react-router";
import { RentalAgreement } from "../../contract";
import Header from "../../Header";
import { Authentication, RoomInfo } from "../../types";
import { makeDomain } from "../../utils";
import useCreateTicket from "./useCreateTicket";

interface Props {
  authentication: Authentication;
  room: RoomInfo;
  signer: Signer;
}

const TYPES = {
  Ticket: [
    { name: `deadline`, type: `uint256` },
    { name: `nonce`, type: `uint256` },
    { name: `value`, type: `uint256` },
  ],
};

interface Ticket {
  deadline: number;
  nonce: bigint;
  value: bigint;
}

interface EditedTicket {
  deadline: string;
  value: string;
}

const CreateTicket = ({ authentication, room, signer }: Props) => {
  const navigate = useNavigate();

  const contract = useMemo(
    () => new RentalAgreement({ address: room.contractAddress!, signer }),
    [room.contractAddress, signer]
  );
  const domain = useMemo(
    () => makeDomain(room.contractAddress!),
    [room.contractAddress]
  );

  const [nonce, setNonce] = useState(``);
  useEffect(() => {
    let isCancelled = false;

    contract.getCashierNonce(authentication.address).then((nonce) => {
      if (!isCancelled) {
        setNonce(nonce);
      }
    });

    return () => {
      isCancelled = true;
    };
  }, [authentication.address, contract]);

  const [editedTicket, setEditedTicket] = useState<EditedTicket>({
    deadline: ``,
    value: ``,
  });

  const createTicket = useCreateTicket({ roomId: room.id });
  const handleSubmit = useCallback(
    async (event: FormEvent) => {
      event.preventDefault();

      const deadline = new Date(editedTicket.deadline);
      const ticket: Ticket = {
        deadline: Number(deadline) / 1000,
        nonce: BigInt(nonce),
        value: BigInt(editedTicket.value),
      };

      // @ts-expect-error: `_signTypedData` is experimental, but it exists
      const rawSignature = await signer._signTypedData(domain, TYPES, ticket);
      const { r, s, v } = utils.splitSignature(rawSignature);
      const signature = { r, s, v: `0x${v.toString(16)}` };

      const ticketId = await createTicket({
        nonce: ticket.nonce,
        value: ticket.value,
        deadline,
        signature,
      });
      navigate(`/ticket/${ticketId}`);
    },
    [editedTicket, nonce, signer, domain, createTicket, navigate]
  );

  return (
    <>
      <Header title="Create a ticket" />
      <form className="form" onSubmit={handleSubmit}>
        <label htmlFor="deadline" className="form__label">
          Deadline
        </label>
        <div className="form__control">
          <input
            className="input"
            type="datetime-local"
            id="deadline"
            value={editedTicket.deadline}
            onChange={(event) =>
              setEditedTicket({
                ...editedTicket,
                deadline: event.currentTarget.value,
              })
            }
            required={true}
          />
        </div>

        <label htmlFor="tenant" className="form__label">
          Nonce
        </label>
        <div className="form__control">
          <input
            className="input"
            type="text"
            id="tenant"
            value={nonce}
            readOnly={true}
          />
        </div>

        <label htmlFor="rental-rate" className="form__label">
          Value
        </label>
        <div className="form__control">
          <input
            className="input"
            type="text"
            id="rental-rate"
            value={editedTicket.value}
            onChange={(event) =>
              setEditedTicket({
                ...editedTicket,
                value: event.currentTarget.value,
              })
            }
            required={true}
          />
          wei
        </div>

        <button type="submit" className="button form__submit">
          Create
        </button>
      </form>
    </>
  );
};

export default CreateTicket;
