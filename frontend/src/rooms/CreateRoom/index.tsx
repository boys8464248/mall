import { useNavigate } from "react-router";
import { useCallback, useState } from "react";

import useCreateRoom from "./useCreateRoom";
import RoomForm, { PartialRoom } from "../RoomForm";
import Header from "../../Header";
import { handleError } from "../../errors";

export const CreateRoom = () => {
  const navigate = useNavigate();

  const [room, setRoom] = useState<PartialRoom>({
    internalName: null,
    area: null,
    location: null,
  });

  const createRoom = useCreateRoom();
  const handleSubmit = useCallback(
    async (completeRoom) => {
      try {
        const room = await createRoom(completeRoom);
        navigate(`/room/${room.id}`);
      } catch (error) {
        handleError(error);
      }
    },
    [createRoom, navigate]
  );

  return (
    <>
      <Header title="Create a room" />
      <RoomForm
        room={room}
        onChange={setRoom}
        onSubmit={handleSubmit}
        submitText="Create"
      />
    </>
  );
};
