import { gql, useMutation } from "@apollo/client";
import { useCallback } from "react";
import { InputRoom } from "../../types";

const CREATE_ROOM = gql`
  mutation ($room: InputRoom!) {
    room: createRoom(room: $room) {
      id
    }
  }
`;

interface Variables {
  room: InputRoom;
}

interface Result {
  room: { id: string };
}

const useCreateRoom = () => {
  const [createRoom] = useMutation<Result, Variables>(CREATE_ROOM);

  return useCallback(
    async (room: InputRoom) => {
      const { data, errors } = await createRoom({ variables: { room } });
      if (errors) {
        throw errors[0];
      }

      return data!.room;
    },
    [createRoom]
  );
};

export default useCreateRoom;
