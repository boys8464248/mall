import { useCallback } from "react";
import { InputRoom } from "../../types";

export type PartialRoom = { [P in keyof InputRoom]: InputRoom[P] | null };

interface Props {
  room: PartialRoom;
  onChange(room: PartialRoom): void;
  onSubmit(room: InputRoom): void;
  submitText: string;
}

const isComplete = (room: PartialRoom): room is InputRoom =>
  Object.values(room).every((x) => x !== null);

const RoomForm = ({ room, onChange, onSubmit, submitText }: Props) => {
  const handleSubmit = useCallback(
    (event) => {
      event.preventDefault();

      if (isComplete(room)) {
        onSubmit(room);
      }
    },
    [onSubmit, room]
  );

  return (
    <form className="form room-form" onSubmit={handleSubmit}>
      <label htmlFor="internal-name" className="form__label">
        Internal name
      </label>
      <div className="form__control">
        <input
          id="internal-name"
          type="text"
          className="input room-form__internal-name"
          value={room.internalName ?? ``}
          onChange={(event) =>
            onChange({
              ...room,
              internalName: event.target.value,
            })
          }
          required={true}
        />
      </div>

      <label htmlFor="area" className="form__label">
        Area
      </label>
      <div className="form__control">
        <input
          id="area"
          type="number"
          className="input room-form__area"
          value={room.area ?? ``}
          onChange={(event) =>
            onChange({
              ...room,
              area: event.target.valueAsNumber,
            })
          }
          min={0}
          required={true}
        />
        sq.m.
      </div>

      <label htmlFor="location" className="form__label">
        Location
      </label>
      <div className="form__control">
        <input
          id="location"
          type="text"
          className="input room-form__location"
          value={room.location ?? ``}
          onChange={(event) =>
            onChange({
              ...room,
              location: event.target.value,
            })
          }
          required={true}
        />
      </div>

      <button type="submit" className="button form__submit room-form__submit">
        {submitText}
      </button>
    </form>
  );
};

export default RoomForm;
