import "./style.css";

import Header from "../../Header";
import AddCashier from "./AddCashier";
import { useCallback, useEffect, useMemo, useState } from "react";
import { RoomInfo } from "../../types";
import { Signer } from "ethers";
import { RentalAgreement } from "../../contract";

interface Props {
  room: RoomInfo;
  signer: Signer;
}

const Cashiers = ({ room, signer }: Props) => {
  const contract = useMemo(
    () => new RentalAgreement({ address: room.contractAddress!, signer }),
    [room.contractAddress, signer]
  );

  const [cashiers, setCashiers] = useState<Set<string> | null>(null);
  useEffect(() => {
    let isCancelled = false;
    contract.getCashiersList().then((cashiers) => {
      if (!isCancelled) {
        setCashiers(cashiers);
      }
    });

    return () => {
      isCancelled = true;
    };
  }, [contract]);

  const handleCashierAdded = useCallback(
    (cashier: string) => {
      setCashiers(new Set([...cashiers!, cashier]));
    },
    [cashiers]
  );
  const removeCashier = useCallback(
    async (cashier: string) => {
      await contract.removeCashier(cashier);

      const newCashiers = new Set(cashiers);
      newCashiers.delete(cashier);
      setCashiers(newCashiers);
    },
    [contract, cashiers]
  );

  if (!cashiers) {
    return null;
  }

  return (
    <>
      <Header title="Cashiers" />
      <AddCashier contract={contract} onCashierAdded={handleCashierAdded} />
      <ul className="cashiers">
        {[...cashiers].map((cashier) => (
          <li className="cashier" key={cashier}>
            <span className="cashier__address">{cashier}</span>
            <button
              className="button button--dangerous cashier__remove"
              onClick={() => removeCashier(cashier)}
            >
              Remove
            </button>
          </li>
        ))}
      </ul>
    </>
  );
};

export default Cashiers;
