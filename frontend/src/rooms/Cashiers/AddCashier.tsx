import { useCallback, useState } from "react";
import { RentalAgreement } from "../../contract";

interface Props {
  contract: RentalAgreement;
  onCashierAdded(cashier: string): void;
}

const AddCashier = ({ contract, onCashierAdded }: Props) => {
  const [cashier, setCashier] = useState(``);
  const handleSubmit = useCallback(
    async (event: React.FormEvent) => {
      event.preventDefault();
      await contract.addCashier(cashier);
      onCashierAdded(cashier);
      setCashier(``);
    },
    [cashier, contract, onCashierAdded]
  );

  return (
    <form className="form add-cashier" onSubmit={handleSubmit}>
      <label className="form__label" htmlFor="address">
        New cashier
      </label>
      <div className="form__control">
        <input
          className="input"
          type="text"
          id="address"
          value={cashier}
          onChange={(event) => setCashier(event.currentTarget.value)}
          required={true}
          minLength={42}
          maxLength={42}
        />
      </div>
      <button className="button form__submit add-cashier__submit">Add</button>
    </form>
  );
};

export default AddCashier;
