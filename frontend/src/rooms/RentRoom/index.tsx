import { Signer } from "ethers";
import { useCallback, useState } from "react";
import { useNavigate } from "react-router";
import { RentalAgreement } from "../../contract";
import Header from "../../Header";
import { RentParameters, RoomInfo } from "../../types";

const readFile = (file: File) =>
  new Promise<string>((resolve) => {
    const reader = new FileReader();
    reader.onload = () => {
      resolve(reader.result as string);
    };
    reader.readAsText(file);
  });

interface Props {
  room: RoomInfo;
  signer: Signer;
  refetchRoom(): Promise<void>;
}

const RentRoom = ({ room, signer, refetchRoom }: Props) => {
  const navigate = useNavigate();

  const [signatureFile, setSignatureFile] = useState<File | null>(null);
  const handleSubmit = useCallback(
    async (event: React.FormEvent) => {
      event.preventDefault();
      const fileContents = await readFile(signatureFile!);
      const rentParameters: RentParameters = JSON.parse(fileContents);

      const contract = new RentalAgreement({
        address: room.contractAddress!,
        signer,
      });
      await contract.rent(rentParameters);
      await refetchRoom();
      navigate(-1);
    },
    [navigate, refetchRoom, room.contractAddress, signatureFile, signer]
  );

  return (
    <>
      <Header title="Rent the room" />
      <form className="form" onSubmit={handleSubmit}>
        <label htmlFor="landlord-sign" className="form__label">
          Landlord signature
        </label>
        <div className="form__control">
          <input
            className="input"
            type="file"
            id="landlord-sign"
            onChange={(event) =>
              setSignatureFile(event.currentTarget.files![0])
            }
            required={true}
          />
        </div>

        <button type="submit" className="button form__submit">
          Rent
        </button>
      </form>
    </>
  );
};

export default RentRoom;
