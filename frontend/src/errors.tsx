import { ApolloError } from "@apollo/client";

const displayError = (error: Error) => {
  const errorElement = document.createElement(`div`);
  errorElement.classList.add(`error`);
  errorElement.textContent = error.message;
  errorElement.addEventListener(`click`, () => errorElement.remove());

  document.body.append(errorElement);
};

export const handleError = (error: unknown) => {
  console.error(error);

  if (
    error instanceof ApolloError &&
    error.networkError !== null &&
    `result` in error.networkError
  ) {
    displayError(error.networkError.result.errors[0]);
  } else if (error instanceof Error) {
    displayError(error);
  } else {
    displayError(new Error(String(error)));
  }
};
