import "./style.css";

import { providers, utils } from "ethers";
import { useCallback } from "react";

import useFetchMessage from "./useFetchMessage";
import useAuthenticate from "./useAuthenticate";
import { Authentication as AuthenticationInfo } from "../types";
import { handleError } from "../errors";

interface Props {
  connect(): Promise<string>;
  provider: providers.Web3Provider;
  status: `unauthenticated` | `outdated`;
  onAuthenticated(authentication: AuthenticationInfo): void;
}

const Authentication = ({ connect, provider, status, onAuthenticated }: Props) => {
  const fetchMessage = useFetchMessage();
  const authenticate = useAuthenticate();
  const handleAuthenticate = useCallback(async () => {
    try {
      const address = await connect();
      const message = await fetchMessage(address);
      const signedMessage = await provider.getSigner().signMessage(message);
      const { r, s, v } = utils.splitSignature(signedMessage);
      const signature = { r, s, v: `0x${v.toString(16)}` };

      const authentication = await authenticate({ address, signature });
      onAuthenticated(authentication);
    } catch (error) {
      handleError(error);
    }
  }, [connect, fetchMessage, provider, authenticate, onAuthenticated]);

  return (
    <div className="authentication">
      <header className="authentication__header">
        {status === `unauthenticated` ? (
          <h1 className="authentication__heading">
            Authenticate with your MetaMask account
          </h1>
        ) : (
          <>
            <h1 className="authentication__warning">
              Your MetaMask account is different from the one you authenticated
              with before
            </h1>
            <p className="authentication__help">
              Authenticate with your current account or switch to the
              authenticated one
            </p>
          </>
        )}
      </header>
      <button
        className="button button--big authentication__authenticate"
        onClick={handleAuthenticate}
      >
        Authenticate
      </button>
    </div>
  );
};

export default Authentication;
