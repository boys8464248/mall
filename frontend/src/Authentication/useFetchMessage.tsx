import { gql, useMutation } from "@apollo/client";
import { useCallback } from "react";

const FETCH_MESSAGE = gql`
  mutation ($address: String!) {
    message: requestAuthentication(address: $address)
  }
`;

interface Variables {
  address: string;
}

interface Result {
  message: string;
}

const useFetchMessage = () => {
  const [fetchMessage] = useMutation<Result, Variables>(FETCH_MESSAGE);

  return useCallback(
    async (address: string) => {
      const { data, errors } = await fetchMessage({ variables: { address } });
      if (errors) {
        throw errors[0];
      }
      return data!.message;
    },
    [fetchMessage]
  );
};

export default useFetchMessage;
