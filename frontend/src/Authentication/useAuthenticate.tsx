import { gql, useMutation } from "@apollo/client";
import { useCallback } from "react";
import { Authentication, Signature } from "../types";

const AUTHENTICATE = gql`
  mutation ($address: String!, $signature: InputSignature!) {
    authentication: authenticate(address: $address, signedMessage: $signature) {
      address
      isLandlord
    }
  }
`;

interface Variables {
  address: string;
  signature: Signature;
}

interface Result {
  authentication: Authentication;
}

const useAuthenticate = () => {
  const [authenticate] = useMutation<Result, Variables>(AUTHENTICATE);

  return useCallback(
    async (variables: Variables) => {
      const { data, errors } = await authenticate({ variables });
      if (errors) {
        throw errors[0];
      }
      return data!.authentication;
    },
    [authenticate]
  );
};

export default useAuthenticate;
